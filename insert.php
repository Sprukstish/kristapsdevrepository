<?php
/* Attempt MySQL server connection. Assuming you are running MySQL
server with default setting (user 'root' with no password) */
$link = mysqli_connect("localhost", "root", "", "productdatabase");
 
// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}
 
// Escape user inputs for security
$SKU = mysqli_real_escape_string($link, $_REQUEST['SKU']);
$Name = mysqli_real_escape_string($link, $_REQUEST['Name']);
$Type = mysqli_real_escape_string($link, $_REQUEST['Type']);
$Price = mysqli_real_escape_string($link, $_REQUEST['Price']);
$Value = mysqli_real_escape_string($link, $_REQUEST['Value']);
$Width = mysqli_real_escape_string($link, $_REQUEST['Width']);
$Height = mysqli_real_escape_string($link, $_REQUEST['Height']);
$Length = mysqli_real_escape_string($link, $_REQUEST['Length']);


// Attempt insert query execution
$sql = "INSERT INTO products (SKU, Name, Price, Type, Value, Width, Height, Length) VALUES ('$SKU', '$Name', '$Price', '$Type', '$Value','$Width','$Height', '$Length')";
if(mysqli_query($link, $sql)){
    header("Location:index.php");
} else{
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
}
 
// Close connection
mysqli_close($link);
?>